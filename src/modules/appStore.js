import router from "../router";
import api from "../plugins/api";

export default {
  namespaced: true,
  state: {
    loggedIn: false,
    api_login_route: "/user/login",
    api_check_token: "/check-token"
  },
  mutations: {
    SET_LOGGED_IN(state, payload) {
      state.loggedIn = true;
      localStorage.setItem("token", payload);
      router.push("home");
    },
    SET_LOGGED_OUT(state, payload) {
      state.loggedIn = false;
      localStorage.setItem("token", "");
      router.push("login");
    }
  },
  actions: {
    SET_LOGGED_IN({ commit, state }, data) {
      return api
        .post(state.api_login_route, {
          email: data.email,
          password: data.password
        })
        .then(result => {
          if (result.data.token) commit("SET_LOGGED_IN", result.data.token);
          return result;
        })
        .catch(err => {
          throw err;
        });
    },
    SET_LOGGED_OUT({ commit }, value) {
      commit("SET_LOGGED_OUT", value);
    },
    CHECK_TOKEN({ state }) {
      return api
        .post(state.api_check_token, {
          check:true
        })
        .then(result => {
          if (result.data.verified) 
            return true;
          else 
            return false;
        })
        .catch(err => {
          //console.log("CHECK_TOKEN.err", err);
          throw err;
        });
    }
  }
};
