import api from "../plugins/api";

export default {
  namespaced: true,
  state: {
    api_route: "/user/list",
    users: []
  },
  getters: {},
  mutations: {
    SET_USER_LIST(state, payload) {
      state.users = payload;
    }
  },
  actions: {
    async GET_USER_LIST({ commit, state }, params) {
      await api
        .post(state.api_route)
        .then(result => {
          commit("SET_USER_LIST", result.data);
        })
        .catch(err => {
          throw err;
        });
    }
  }
};
