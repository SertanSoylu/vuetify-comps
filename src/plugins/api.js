import Vue from 'vue';
import config from '../config';
import axios from 'axios';

const api = axios.create({
  baseURL : config.API_URL,
  headers : {
    Autherization: `Bearer ${localStorage.getItem("token")}`
  } 
});

Vue.prototype.$api = api; 


export default api;