/* eslint-disable */

import Vue from "vue";
import Router from "vue-router";
import store from "./store";

import Login from "./components/Login.vue";
import Register from "./components/Register.vue";
import Home from "./views/Home.vue";
import About from "./views/About.vue";

Vue.use(Router);

const router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/home",
      name: "home",
      component: Home
    },
    {
      path: "/about",
      name: "about",
      component: About
    },
    {
      path: "/login",
      name: "login",
      component: Login
    },
    {
      path: "/register",
      name: "register",
      component: Register
    }
  ]
});

router.beforeEach((to, from, next) => {
  //do not check logging
  if (to.name.toUpperCase() === "LOGIN" || to.name.toUpperCase() === "REGISTER") {
    next();
    return;
  }

  if (!store.state.appStore.loggedIn) {
    next("login");
  } else {
    store
      .dispatch("appStore/CHECK_TOKEN")
      .then(res => {
        if (res) {
          next();
        }          
        else {
          next("login");          
        }
      })
      .catch(err => {
        alert(err);
        next("login");
      });
  }
});

export default router;
